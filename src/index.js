import _ from 'lodash';
import * as echarts from 'echarts';
var $ = require( "jquery" );
var bs = require('bootstrap');


var chartDom = document.getElementById('main');
var myChart = echarts.init(chartDom);
var option;



$.get('fedoraroles.svg', function (svg) {
    echarts.registerMap('fedora_org', {svg: svg});

    option = {
        tooltip: {
        },
       
        geo: {
            map: 'fedora_org',
            roam: true,
            emphasis: {
                focus: 'self',
                itemStyle: {
                    color: null
                },
                label: {
                    position: 'bottom',
                    distance: 0,
                    textBorderColor: '#fff',
                    textBorderWidth: 2
                }
            },
            blur: {
            },

        },
    };

    myChart.setOption(option);

    myChart.on('mouseover', { seriesIndex: 0 }, function (event) {
        myChart.dispatchAction({
            type: 'highlight',
            geoIndex: 0,
            name: event.name
        });
    });

    myChart.on('mouseout', { seriesIndex: 0 }, function (event) {
        myChart.dispatchAction({
            type: 'downplay',
            geoIndex: 0,
            name: event.name

        });
    


    });


    myChart.on('click', function(event) {

        var winm = document.getElementById('windmode');

        var windmode = new bs.Modal(
            winm, {
                keyboard: true
            }
        );

       
            console.dir(event);
            document.getElementById("modename").innerText = event.name;
            document.getElementById("modedesc").innerText = "";
            document.getElementById("modextra").innerText = event.name;
            windmode.show();
        
    });

});


option && myChart.setOption(option);
